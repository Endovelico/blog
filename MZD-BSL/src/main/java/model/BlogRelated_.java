package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-28T19:03:38.084+0000")
@StaticMetamodel(BlogRelated.class)
public class BlogRelated_ {
	public static volatile SingularAttribute<BlogRelated, BlogRelatedPK> id;
	public static volatile SingularAttribute<BlogRelated, BlogPost> blogPost;
}
