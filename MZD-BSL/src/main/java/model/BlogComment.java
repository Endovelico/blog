package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the blog_comment database table.
 * 
 */
@Entity
@Table(name="blog_comment")
@NamedQuery(name="BlogComment.findAll", query="SELECT b FROM BlogComment b")
public class BlogComment implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private BlogCommentPK id;

	@Lob
	private String comment;

	private Timestamp date;

	private byte enabled;

	@Column(name="is_reply_to_id")
	private int isReplyToId;

	@Column(name="mark_read")
	private byte markRead;

	//bi-directional many-to-one association to BlogPost
	@ManyToOne
	@JoinColumn(name="post_id")
	private BlogPost blogPost;

	//bi-directional many-to-one association to BlogUser
	@ManyToOne
	@JoinColumn(name="user_id")
	private BlogUser blogUser;

	public BlogComment() {
	}

	public BlogCommentPK getId() {
		return this.id;
	}

	public void setId(BlogCommentPK id) {
		this.id = id;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Timestamp getDate() {
		return this.date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public byte getEnabled() {
		return this.enabled;
	}

	public void setEnabled(byte enabled) {
		this.enabled = enabled;
	}

	public int getIsReplyToId() {
		return this.isReplyToId;
	}

	public void setIsReplyToId(int isReplyToId) {
		this.isReplyToId = isReplyToId;
	}

	public byte getMarkRead() {
		return this.markRead;
	}

	public void setMarkRead(byte markRead) {
		this.markRead = markRead;
	}

	public BlogPost getBlogPost() {
		return this.blogPost;
	}

	public void setBlogPost(BlogPost blogPost) {
		this.blogPost = blogPost;
	}

	public BlogUser getBlogUser() {
		return this.blogUser;
	}

	public void setBlogUser(BlogUser blogUser) {
		this.blogUser = blogUser;
	}

}