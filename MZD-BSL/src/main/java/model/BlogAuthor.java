package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the blog_author database table.
 * 
 */
@Entity
@Table(name="blog_author")
public class BlogAuthor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="display_name")
	private String displayName;

	@Column(name="first_name")
	private String firstName;

	@Column(name="last_name")
	private String lastName;

	//bi-directional many-to-one association to BlogPost
	@OneToMany(mappedBy="blogAuthor")
	private List<BlogPost> blogPosts;

	public BlogAuthor() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDisplayName() {
		return this.displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public List<BlogPost> getBlogPosts() {
		return this.blogPosts;
	}

	public void setBlogPosts(List<BlogPost> blogPosts) {
		this.blogPosts = blogPosts;
	}

	public BlogPost addBlogPost(BlogPost blogPost) {
		getBlogPosts().add(blogPost);
		blogPost.setBlogAuthor(this);

		return blogPost;
	}

	public BlogPost removeBlogPost(BlogPost blogPost) {
		getBlogPosts().remove(blogPost);
		blogPost.setBlogAuthor(null);

		return blogPost;
	}

}