package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-28T19:03:38.090+0000")
@StaticMetamodel(BlogTag.class)
public class BlogTag_ {
	public static volatile SingularAttribute<BlogTag, Integer> id;
	public static volatile SingularAttribute<BlogTag, String> tag;
	public static volatile SingularAttribute<BlogTag, String> tagClean;
	public static volatile SingularAttribute<BlogTag, BlogPost> blogPost;
}
