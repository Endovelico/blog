package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-28T19:03:38.081+0000")
@StaticMetamodel(BlogCommentPK.class)
public class BlogCommentPK_ {
	public static volatile SingularAttribute<BlogCommentPK, Integer> id;
	public static volatile SingularAttribute<BlogCommentPK, Integer> userId;
}
