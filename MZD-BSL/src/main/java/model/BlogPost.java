package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the blog_post database table.
 * 
 */
@Entity
@Table(name="blog_post")
@NamedQuery(name="BlogPost.findAll", query="SELECT b FROM BlogPost b")
public class BlogPost implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Lob
	private String article;

	@Column(name="banner_image")
	private String bannerImage;

	@Column(name="comments_enabled")
	private byte commentsEnabled;

	@Column(name="date_published")
	private Timestamp datePublished;

	private byte enabled;

	private byte featured;

	private String file;

	private String title;

	@Column(name="title_clean")
	private String titleClean;

	private int views;

	//bi-directional many-to-one association to BlogComment
	@OneToMany(mappedBy="blogPost")
	private List<BlogComment> blogComments;

	//bi-directional many-to-one association to BlogAuthor
	@ManyToOne
	@JoinColumn(name="author_id")
	private BlogAuthor blogAuthor;

	//bi-directional many-to-many association to BlogCategory
	@ManyToMany
	@JoinTable(
		name="blog_post_to_category"
		, joinColumns={
			@JoinColumn(name="post_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="category_id")
			}
		)
	private List<BlogCategory> blogCategories;

	//bi-directional many-to-one association to BlogRelated
	@OneToMany(mappedBy="blogPost")
	private List<BlogRelated> blogRelateds;

	//bi-directional many-to-one association to BlogTag
	@OneToMany(mappedBy="blogPost")
	private List<BlogTag> blogTags;

	public BlogPost() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getArticle() {
		return this.article;
	}

	public void setArticle(String article) {
		this.article = article;
	}

	public String getBannerImage() {
		return this.bannerImage;
	}

	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}

	public byte getCommentsEnabled() {
		return this.commentsEnabled;
	}

	public void setCommentsEnabled(byte commentsEnabled) {
		this.commentsEnabled = commentsEnabled;
	}

	public Timestamp getDatePublished() {
		return this.datePublished;
	}

	public void setDatePublished(Timestamp datePublished) {
		this.datePublished = datePublished;
	}

	public byte getEnabled() {
		return this.enabled;
	}

	public void setEnabled(byte enabled) {
		this.enabled = enabled;
	}

	public byte getFeatured() {
		return this.featured;
	}

	public void setFeatured(byte featured) {
		this.featured = featured;
	}

	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleClean() {
		return this.titleClean;
	}

	public void setTitleClean(String titleClean) {
		this.titleClean = titleClean;
	}

	public int getViews() {
		return this.views;
	}

	public void setViews(int views) {
		this.views = views;
	}

	public List<BlogComment> getBlogComments() {
		return this.blogComments;
	}

	public void setBlogComments(List<BlogComment> blogComments) {
		this.blogComments = blogComments;
	}

	public BlogComment addBlogComment(BlogComment blogComment) {
		getBlogComments().add(blogComment);
		blogComment.setBlogPost(this);

		return blogComment;
	}

	public BlogComment removeBlogComment(BlogComment blogComment) {
		getBlogComments().remove(blogComment);
		blogComment.setBlogPost(null);

		return blogComment;
	}

	public BlogAuthor getBlogAuthor() {
		return this.blogAuthor;
	}

	public void setBlogAuthor(BlogAuthor blogAuthor) {
		this.blogAuthor = blogAuthor;
	}

	public List<BlogCategory> getBlogCategories() {
		return this.blogCategories;
	}

	public void setBlogCategories(List<BlogCategory> blogCategories) {
		this.blogCategories = blogCategories;
	}

	public List<BlogRelated> getBlogRelateds() {
		return this.blogRelateds;
	}

	public void setBlogRelateds(List<BlogRelated> blogRelateds) {
		this.blogRelateds = blogRelateds;
	}

	public BlogRelated addBlogRelated(BlogRelated blogRelated) {
		getBlogRelateds().add(blogRelated);
		blogRelated.setBlogPost(this);

		return blogRelated;
	}

	public BlogRelated removeBlogRelated(BlogRelated blogRelated) {
		getBlogRelateds().remove(blogRelated);
		blogRelated.setBlogPost(null);

		return blogRelated;
	}

	public List<BlogTag> getBlogTags() {
		return this.blogTags;
	}

	public void setBlogTags(List<BlogTag> blogTags) {
		this.blogTags = blogTags;
	}

	public BlogTag addBlogTag(BlogTag blogTag) {
		getBlogTags().add(blogTag);
		blogTag.setBlogPost(this);

		return blogTag;
	}

	public BlogTag removeBlogTag(BlogTag blogTag) {
		getBlogTags().remove(blogTag);
		blogTag.setBlogPost(null);

		return blogTag;
	}

}