package model.repositories;

import model.BlogAuthor;

import org.springframework.data.repository.CrudRepository;

public interface BlogAuthorRepository extends CrudRepository<BlogAuthor, Integer> {
    
    //MILESTONE: BEHOLD OUR VERY FIRST METHOD
	BlogAuthor findByDisplayName(String displayName);
}