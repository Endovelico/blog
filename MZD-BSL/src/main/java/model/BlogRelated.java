package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the blog_related database table.
 * 
 */
@Entity
@Table(name="blog_related")
@NamedQuery(name="BlogRelated.findAll", query="SELECT b FROM BlogRelated b")
public class BlogRelated implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private BlogRelatedPK id;

	//bi-directional many-to-one association to BlogPost
	@ManyToOne
	@JoinColumn(name="blog_post_id")
	private BlogPost blogPost;

	public BlogRelated() {
	}

	public BlogRelatedPK getId() {
		return this.id;
	}

	public void setId(BlogRelatedPK id) {
		this.id = id;
	}

	public BlogPost getBlogPost() {
		return this.blogPost;
	}

	public void setBlogPost(BlogPost blogPost) {
		this.blogPost = blogPost;
	}

}