package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the blog_tag database table.
 * 
 */
@Entity
@Table(name="blog_tag")
@NamedQuery(name="BlogTag.findAll", query="SELECT b FROM BlogTag b")
public class BlogTag implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String tag;

	@Column(name="tag_clean")
	private String tagClean;

	//bi-directional many-to-one association to BlogPost
	@ManyToOne
	@JoinColumn(name="post_id")
	private BlogPost blogPost;

	public BlogTag() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTag() {
		return this.tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getTagClean() {
		return this.tagClean;
	}

	public void setTagClean(String tagClean) {
		this.tagClean = tagClean;
	}

	public BlogPost getBlogPost() {
		return this.blogPost;
	}

	public void setBlogPost(BlogPost blogPost) {
		this.blogPost = blogPost;
	}

}