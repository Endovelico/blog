package model;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-28T19:03:38.082+0000")
@StaticMetamodel(BlogPost.class)
public class BlogPost_ {
	public static volatile SingularAttribute<BlogPost, Integer> id;
	public static volatile SingularAttribute<BlogPost, String> article;
	public static volatile SingularAttribute<BlogPost, String> bannerImage;
	public static volatile SingularAttribute<BlogPost, Byte> commentsEnabled;
	public static volatile SingularAttribute<BlogPost, Timestamp> datePublished;
	public static volatile SingularAttribute<BlogPost, Byte> enabled;
	public static volatile SingularAttribute<BlogPost, Byte> featured;
	public static volatile SingularAttribute<BlogPost, String> file;
	public static volatile SingularAttribute<BlogPost, String> title;
	public static volatile SingularAttribute<BlogPost, String> titleClean;
	public static volatile SingularAttribute<BlogPost, Integer> views;
	public static volatile ListAttribute<BlogPost, BlogComment> blogComments;
	public static volatile SingularAttribute<BlogPost, BlogAuthor> blogAuthor;
	public static volatile ListAttribute<BlogPost, BlogCategory> blogCategories;
	public static volatile ListAttribute<BlogPost, BlogRelated> blogRelateds;
	public static volatile ListAttribute<BlogPost, BlogTag> blogTags;
}
