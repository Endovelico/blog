package model;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-28T19:03:38.065+0000")
@StaticMetamodel(BlogComment.class)
public class BlogComment_ {
	public static volatile SingularAttribute<BlogComment, BlogCommentPK> id;
	public static volatile SingularAttribute<BlogComment, String> comment;
	public static volatile SingularAttribute<BlogComment, Timestamp> date;
	public static volatile SingularAttribute<BlogComment, Byte> enabled;
	public static volatile SingularAttribute<BlogComment, Integer> isReplyToId;
	public static volatile SingularAttribute<BlogComment, Byte> markRead;
	public static volatile SingularAttribute<BlogComment, BlogPost> blogPost;
	public static volatile SingularAttribute<BlogComment, BlogUser> blogUser;
}
