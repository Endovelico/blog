package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-28T19:03:38.092+0000")
@StaticMetamodel(BlogUser.class)
public class BlogUser_ {
	public static volatile SingularAttribute<BlogUser, Integer> id;
	public static volatile SingularAttribute<BlogUser, String> email;
	public static volatile SingularAttribute<BlogUser, String> name;
	public static volatile SingularAttribute<BlogUser, String> website;
	public static volatile ListAttribute<BlogUser, BlogComment> blogComments;
}
