package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the blog_user database table.
 * 
 */
@Entity
@Table(name="blog_user")
@NamedQuery(name="BlogUser.findAll", query="SELECT b FROM BlogUser b")
public class BlogUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String email;

	private String name;

	private String website;

	//bi-directional many-to-one association to BlogComment
	@OneToMany(mappedBy="blogUser")
	private List<BlogComment> blogComments;

	public BlogUser() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWebsite() {
		return this.website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public List<BlogComment> getBlogComments() {
		return this.blogComments;
	}

	public void setBlogComments(List<BlogComment> blogComments) {
		this.blogComments = blogComments;
	}

	public BlogComment addBlogComment(BlogComment blogComment) {
		getBlogComments().add(blogComment);
		blogComment.setBlogUser(this);

		return blogComment;
	}

	public BlogComment removeBlogComment(BlogComment blogComment) {
		getBlogComments().remove(blogComment);
		blogComment.setBlogUser(null);

		return blogComment;
	}

}