package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the blog_category database table.
 * 
 */
@Entity
@Table(name="blog_category")
@NamedQuery(name="BlogCategory.findAll", query="SELECT b FROM BlogCategory b")
public class BlogCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Column(name="date_created")
	private Timestamp dateCreated;

	private byte enabled;

	private String name;

	@Column(name="name_clean")
	private String nameClean;

	//bi-directional many-to-many association to BlogPost
	@ManyToMany(mappedBy="blogCategories")
	private List<BlogPost> blogPosts;

	public BlogCategory() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	public byte getEnabled() {
		return this.enabled;
	}

	public void setEnabled(byte enabled) {
		this.enabled = enabled;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameClean() {
		return this.nameClean;
	}

	public void setNameClean(String nameClean) {
		this.nameClean = nameClean;
	}

	public List<BlogPost> getBlogPosts() {
		return this.blogPosts;
	}

	public void setBlogPosts(List<BlogPost> blogPosts) {
		this.blogPosts = blogPosts;
	}

}