package model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the blog_related database table.
 * 
 */
@Embeddable
public class BlogRelatedPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="blog_post_id", insertable=false, updatable=false)
	private int blogPostId;

	@Column(name="blog_related_post_id")
	private int blogRelatedPostId;

	public BlogRelatedPK() {
	}
	public int getBlogPostId() {
		return this.blogPostId;
	}
	public void setBlogPostId(int blogPostId) {
		this.blogPostId = blogPostId;
	}
	public int getBlogRelatedPostId() {
		return this.blogRelatedPostId;
	}
	public void setBlogRelatedPostId(int blogRelatedPostId) {
		this.blogRelatedPostId = blogRelatedPostId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof BlogRelatedPK)) {
			return false;
		}
		BlogRelatedPK castOther = (BlogRelatedPK)other;
		return 
			(this.blogPostId == castOther.blogPostId)
			&& (this.blogRelatedPostId == castOther.blogRelatedPostId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.blogPostId;
		hash = hash * prime + this.blogRelatedPostId;
		
		return hash;
	}
}