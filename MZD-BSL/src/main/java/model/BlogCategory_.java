package model;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-28T19:03:38.063+0000")
@StaticMetamodel(BlogCategory.class)
public class BlogCategory_ {
	public static volatile SingularAttribute<BlogCategory, Integer> id;
	public static volatile SingularAttribute<BlogCategory, Timestamp> dateCreated;
	public static volatile SingularAttribute<BlogCategory, Byte> enabled;
	public static volatile SingularAttribute<BlogCategory, String> name;
	public static volatile SingularAttribute<BlogCategory, String> nameClean;
	public static volatile ListAttribute<BlogCategory, BlogPost> blogPosts;
}
