package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2015-02-28T19:03:37.395+0000")
@StaticMetamodel(BlogAuthor.class)
public class BlogAuthor_ {
	public static volatile SingularAttribute<BlogAuthor, Integer> id;
	public static volatile SingularAttribute<BlogAuthor, String> displayName;
	public static volatile SingularAttribute<BlogAuthor, String> firstName;
	public static volatile SingularAttribute<BlogAuthor, String> lastName;
	public static volatile ListAttribute<BlogAuthor, BlogPost> blogPosts;
}
